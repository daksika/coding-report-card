import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Login extends JFrame implements ActionListener {

    JLabel title, username, password, alert;
    JTextArea username_text;
    JPasswordField word_text;
    JButton login_btn;

    java.util.List<String> st_name = AddStudent.getStudent_name();

    public Login() {
        super("Report Card");
        setSize(500, 500);
        setLocation(250, 250);
        setResizable(false);
        setLayout(null);

        title =new JLabel("Login");
        title.setBounds(220,50,300,30);
        title.setFont(new Font("Serif", Font.PLAIN, 24));
        add(title);


        username = new JLabel("Username");
        username.setBounds(75, 150, 300, 20);
        username.setFont(new Font("Serif", Font.PLAIN, 20));
        add(username);

        username_text = new JTextArea();
        username_text.getText();
        username_text.setBounds(225, 150, 200, 20);
        username_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(username_text);


        password = new JLabel("Password");
        password.setBounds(75, 200,300, 20);
        password.setFont(new Font("Serif", Font.PLAIN, 20));
        add(password);

        word_text = new JPasswordField();
        word_text.setBounds(225, 200, 200, 20);
        word_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(word_text);


        alert = new JLabel("Enter username and password to login");
        alert.setBounds(50, 275, 400, 20);
        alert.setFont(new Font("Serif", Font.PLAIN, 20));
        add(alert);


        login_btn = new JButton("Login (=>)");
        login_btn.setBounds(150, 350, 200, 30);
        login_btn.setFont(new Font("Serif", Font.BOLD, 20));
        add(login_btn);
        login_btn.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if(obj== login_btn)
        {
            login();
        }

    }

    private void login() {
        
        String uname = username_text.getText();
        String pword = word_text.getText();

        if(uname.equals("admin"))
        {
            if(pword.equals("admin"))
            {
                Teacher teacher = new Teacher();
                this.setVisible(false);
                teacher.setVisible(true);
            }
            else
            {
                alert.setText("Entered password is wrong");
            }
        }
        else
        {
            if(st_name.contains(uname))
            {
                if(pword.equals(uname)){
                    Student student = new Student(uname);
                    student.setVisible(true);
                    this.setVisible(false);
                }
                else
                {
                    alert.setText("Entered password is wrong");
                }
            }
            else
            {
                alert.setText("Entered username is wrong");
            }
        }

    }

}

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame implements ActionListener {

    JLabel title,name,st_name;
    JTextArea name_text;
    JButton go,back;
    JLabel subject,marks,grade,maths,maths_marks,maths_grade,science,science_marks,science_grade,tamil,tamil_marks,tamil_grade,english,english_marks,english_grade;

    java.util.List<String> temp_name = AddStudent.getStudent_name();

    java.util.List<Integer> temp_maths = AddStudent.getMaths_list();
    java.util.List<Integer> temp_science = AddStudent.getScience_list();
    java.util.List<Integer> temp_tamil = AddStudent.getTamil_list();
    java.util.List<Integer> temp_english = AddStudent.getEnglish_list();

    View(){

        super("Report Card");
        setSize(500, 500);
        setLocation(250, 250);
        setResizable(false);
        setLayout(null);

        title = new JLabel("View Student Details");
        title.setBounds(120,25,300,50);
        title.setFont(new Font("Serif", Font.PLAIN, 24));
        add(title);

        name = new JLabel("Name");
        name.setBounds(75, 100, 300, 20);
        name.setFont(new Font("Serif", Font.PLAIN, 20));
        add(name);

        name_text = new JTextArea();
        name_text.setBounds(180, 100, 200, 20);
        name_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(name_text);

        go = new JButton("Go >");
        go.setBounds(400,100,70,20);
        go.addActionListener(this);
        add(go);

        st_name = new JLabel();
        st_name.setText("Student Name");
        st_name.setBounds(50, 150, 300, 20);
        st_name.setFont(new Font("Serif", Font.BOLD, 20));
        add(st_name);

        subject = new JLabel();
        subject.setText("Subject");
        subject.setBounds(50, 200, 300, 20);
        subject.setFont(new Font("Serif", Font.BOLD, 20));
        add(subject);

        marks = new JLabel();
        marks.setText("Marks");
        marks.setBounds(200, 200, 300, 20);
        marks.setFont(new Font("Serif", Font.BOLD, 20));
        add(marks);

        grade = new JLabel();
        grade.setText("Grade");
        grade.setBounds(350, 200, 300, 20);
        grade.setFont(new Font("Serif", Font.BOLD, 20));
        add(grade);

        maths = new JLabel();
        maths.setText("Maths");
        maths.setBounds(50, 250, 300, 20);
        maths.setFont(new Font("Serif", Font.BOLD, 20));
        add(maths);

        maths_marks = new JLabel();
        maths_marks.setBounds(200, 250, 300, 20);
        maths_marks.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths_marks);

        maths_grade = new JLabel();
        maths_grade.setBounds(350, 250, 300, 20);
        maths_grade.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths_grade);

        science = new JLabel();
        science.setText("Science");
        science.setBounds(50, 280, 300, 20);
        science.setFont(new Font("Serif", Font.BOLD, 20));
        add(science);

        science_marks = new JLabel();
        science_marks.setBounds(200, 280, 300, 20);
        science_marks.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science_marks);

        science_grade = new JLabel();
        science_grade.setBounds(350, 280, 300, 20);
        science_grade.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science_grade);

        tamil = new JLabel();
        tamil.setText("Tamil");
        tamil.setBounds(50, 310, 300, 20);
        tamil.setFont(new Font("Serif", Font.BOLD, 20));
        add(tamil);

        tamil_marks = new JLabel();
        tamil_marks.setBounds(200, 310, 300, 20);
        tamil_marks.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil_marks);

        tamil_grade = new JLabel();
        tamil_grade.setBounds(350, 310, 300, 20);
        tamil_grade.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil_grade);

        english = new JLabel();
        english.setText("English");
        english.setBounds(50, 340, 300, 20);
        english.setFont(new Font("Serif", Font.BOLD, 20));
        add(english);

        english_marks = new JLabel();
        english_marks.setBounds(200, 340, 300, 20);
        english_marks.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english_marks);

        english_grade = new JLabel();
        english_grade.setBounds(350, 340, 300, 20);
        english_grade.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english_grade);

        back = new JButton("Back (<)");
        back.setBounds(200,400,100,30);
        back.addActionListener(this);
        add(back);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();

        if(obj == go)
        {
            setDetails();
        }

        else if(obj == back)
        {
            Teacher teacher = new Teacher();
            teacher.setVisible(true);
            this.setVisible(false);
        }

    }

    private void setDetails() {
        String stname = name_text.getText();
        int index = temp_name.indexOf(stname);

        st_name.setText(stname);
        maths_marks.setText(String.valueOf(temp_maths.get(index)));
        maths_grade.setText(grade(temp_maths.get(index)));
        science_marks.setText(String.valueOf(temp_science.get(index)));
        science_grade.setText(grade(temp_science.get(index)));
        tamil_marks.setText(String.valueOf(temp_tamil.get(index)));
        tamil_grade.setText(grade(temp_tamil.get(index)));
        english_marks.setText(String.valueOf(temp_english.get(index)));
        english_grade.setText(grade(temp_english.get(index)));

    }

    private String grade(Integer marks) {
        String grade = "";
        if(marks >= 75){
            grade = "A";
        }
        else if(marks >= 65){
            grade = "B";
        }
        else if(marks >= 55){
            grade = "C";
        }
        else if(marks >= 35){
            grade = "D";
        }
        else {
            grade = "F";
        }
        return grade;
    }
}

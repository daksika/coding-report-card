import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Teacher extends JFrame implements ActionListener {

    JLabel title;
    JButton b1,b2,b3,b4,logout,exit;


    Teacher(){

        super("Report Card");
        setSize(500, 500);
        setLocation(250, 250);
        setResizable(false);
        setLayout(null);

        title = new JLabel("Teacher");
        title.setBounds(210,25,100,50);
        title.setFont(new Font("Serif", Font.PLAIN, 24));
        add(title);

        b1 = new JButton("Add Student Details");
        b1.setBounds(140,100,220,30);
        b1.addActionListener(this);
        add(b1);

        b2 = new JButton("View High Rank Holders");
        b2.setBounds(140,160,220,30);
        b2.addActionListener(this);
        add(b2);

        b3 = new JButton("View Student Details");
        b3.setBounds(140,220,220,30);
        b3.addActionListener(this);
        add(b3);

        b4 = new JButton("View All Student Details");
        b4.setBounds(140,280,220,30);
        b4.addActionListener(this);
        add(b4);

        logout = new JButton("Logout (<=)");
        logout.setBounds(100,375,120,30);
        logout.addActionListener(this);
        add(logout);

        exit = new JButton("Exit (X)");
        exit.setBounds(300,375,120,30);
        exit.addActionListener(this);
        add(exit);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

        Object obj = e.getSource();

        if(obj==b1)
        {
            AddStudent addStudent = new AddStudent();
            addStudent.setVisible(true);
            this.setVisible(false);
        }

        else if(obj==b2)
        {
            ViewRank viewRank = new ViewRank();
            viewRank.setVisible(true);
            this.setVisible(false);
        }

        else if(obj==b3)
        {
            View view = new View();
            view.setVisible(true);
            this.setVisible(false);
        }

        else if(obj==b4)
        {
            ViewAll viewAll = new ViewAll();
            viewAll.setVisible(true);
            this.setVisible(false);
        }

        else if(obj==logout)
        {
            Login login = new Login();
            login.setVisible(true);
            this.setVisible(false);
        }

        else if(obj==exit)
        {
            System.exit(0);
        }

    }

}

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

public class ViewRank extends JFrame implements ActionListener {

    JLabel title;
    JLabel subject,name,maths,maths_text,science,science_text,tamil,tamil_text,english,english_text;
    JButton back;

    static java.util.List<String> temp_name = AddStudent.getStudent_name();

    java.util.List<Integer> temp_maths = AddStudent.getMaths_list();
    java.util.List<Integer> temp_science = AddStudent.getScience_list();
    java.util.List<Integer> temp_tamil = AddStudent.getTamil_list();
    java.util.List<Integer> temp_english = AddStudent.getEnglish_list();


    ViewRank(){

        super("Report Card");
        setSize(500, 500);
        setLocation(250, 250);
        setResizable(false);
        setLayout(null);

        title = new JLabel("View High Rank Holders");
        title.setBounds(100,25,300,50);
        title.setFont(new Font("Serif", Font.PLAIN, 24));
        add(title);

        subject = new JLabel("Subjects");
        subject.setBounds(75, 100, 300, 20);
        subject.setFont(new Font("Serif", Font.BOLD, 20));
        add(subject);

        name = new JLabel("High Rank Holders");
        name.getText();
        name.setBounds(225, 100, 250, 20);
        name.setFont(new Font("Serif", Font.BOLD, 20));
        add(name);


        maths = new JLabel("Maths");
        maths.setBounds(75, 150, 300, 20);
        maths.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths);

        maths_text = new JLabel();
        maths_text.setText(high_rank(temp_maths));
        maths_text.setBounds(225, 150, 200, 20);
        maths_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths_text);


        science = new JLabel("Science");
        science.setBounds(75, 200, 300, 20);
        science.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science);

        science_text = new JLabel();
        science_text.setText(high_rank(temp_science));
        science_text.setBounds(225, 200, 200, 20);
        science_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science_text);


        tamil = new JLabel("Tamil");
        tamil.setBounds(75, 250, 300, 20);
        tamil.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil);

        tamil_text = new JLabel();
        tamil_text.setText(high_rank(temp_tamil));
        tamil_text.setBounds(225, 250, 200, 20);
        tamil_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil_text);


        english = new JLabel("English");
        english.setBounds(75, 300, 300, 20);
        english.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english);

        english_text = new JLabel();
        english_text.setText(high_rank(temp_english));
        english_text.setBounds(225, 300, 200, 20);
        english_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english_text);

        back = new JButton("Back (<)");
        back.setBounds(200,380,120,30);
        back.addActionListener(this);
        add(back);
    }

    public String high_rank(List<Integer> array){
        Integer obj = Collections.max(array);
        int index = array.indexOf(obj);
        String name = temp_name.get(index);
        return name;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Object obj = e.getSource();

        if(obj == back)
        {
            Teacher teacher = new Teacher();
            teacher.setVisible(true);
            this.setVisible(false);
        }
    }
}

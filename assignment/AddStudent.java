import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class AddStudent extends JFrame implements ActionListener {

    int counter = 0;
    JLabel title,name,maths,science,tamil,english,info;
    JTextArea name_text,maths_text,science_text,tamil_text,english_text;
    JButton add, back;

    static java.util.List<String> student_name = new ArrayList<String>();

    public static List<Integer> getMaths_list() {
        return maths_list;
    }

    public static List<Integer> getScience_list() {
        return science_list;
    }

    public static List<Integer> getTamil_list() {
        return tamil_list;
    }

    public static List<Integer> getEnglish_list() {
        return english_list;
    }

    static java.util.List<Integer> maths_list = new ArrayList<Integer>();
    static java.util.List<Integer> science_list = new ArrayList<Integer>();
    static java.util.List<Integer> tamil_list = new ArrayList<Integer>();
    static java.util.List<Integer> english_list = new ArrayList<Integer>();


    public static java.util.List<String> getStudent_name() {
        return student_name;
    }

    AddStudent(){

        super("Report Card");
        setSize(500, 500);
        setLocation(250, 250);
        setResizable(false);
        setLayout(null);

        title = new JLabel("Add Student Details");
        title.setBounds(140,25,300,50);
        title.setFont(new Font("Serif", Font.PLAIN, 24));
        add(title);

        name = new JLabel("Name");
        name.setBounds(75, 100, 300, 20);
        name.setFont(new Font("Serif", Font.PLAIN, 20));
        add(name);

        name_text = new JTextArea();
        name_text.getText();
        name_text.setBounds(225, 100, 200, 20);
        name_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(name_text);


        maths = new JLabel("Maths");
        maths.setBounds(75, 150, 300, 20);
        maths.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths);

        maths_text = new JTextArea();
        maths_text.getText();
        maths_text.setBounds(225, 150, 200, 20);
        maths_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(maths_text);


        science = new JLabel("Science");
        science.setBounds(75, 200, 300, 20);
        science.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science);

        science_text = new JTextArea();
        science_text.getText();
        science_text.setBounds(225, 200, 200, 20);
        science_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(science_text);


        tamil = new JLabel("Tamil");
        tamil.setBounds(75, 250, 300, 20);
        tamil.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil);

        tamil_text = new JTextArea();
        tamil_text.getText();
        tamil_text.setBounds(225, 250, 200, 20);
        tamil_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(tamil_text);


        english = new JLabel("English");
        english.setBounds(75, 300, 300, 20);
        english.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english);

        english_text = new JTextArea();
        english_text.getText();
        english_text.setBounds(225, 300, 200, 20);
        english_text.setFont(new Font("Serif", Font.PLAIN, 20));
        add(english_text);

        add = new JButton("Add (+)");
        add.setBounds(100,350,120,30);
        add.addActionListener(this);
        add(add);

        back = new JButton("Back (<)");
        back.setBounds(300,350,120,30);
        back.addActionListener(this);
        add(back);

        info = new JLabel();
        info.setBounds(130, 400, 300, 20);
        info.setFont(new Font("Serif", Font.PLAIN, 20));
        add(info);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Object obj = e.getSource();

        if(obj == add)
        {
            student_name.add(name_text.getText());

            maths_list.add(Integer.valueOf(maths_text.getText()));
            science_list.add(Integer.valueOf(science_text.getText()));
            tamil_list.add(Integer.valueOf(tamil_text.getText()));
            english_list.add(Integer.valueOf(english_text.getText()));
            info.setText(++counter + " Student Detail(s) Added");
        }

        else if(obj == back)
        {
            Teacher teacher = new Teacher();
            teacher.setVisible(true);
            this.setVisible(false);
        }

    }

}
